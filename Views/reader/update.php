<div class="row">
  <div class="col s12 m12">
    <div class="icon-block">
      <h2 class="center light-blue-text"><i class="material-icons">face</i></h2>
      <h5 class="center">Lectores / Editar / <?= $data['nombre_lector'] ?></h5>

      <a href="<?= URL ?>/reader" class="btn-small"><i class="material-icons left">subdirectory_arrow_left</i>Regresar</a>
    
      <br><br>
      <form class="col s12" method="post" action="<?= URL ?>/reader/update/<?= $data['id_lector'] ?>">
        
        <div class="row">
          <div class="input-field col s12">
            <input id="txtName" name="Reader[name]" type="text" value="<?= $data['nombre_lector'] ?>" class="validate" placeholder="Nombre del lector" required autofocus>
            <label for="txtName">Nombre</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="txtCI" name="Reader[ci]" type="text" value="<?= $data['ci'] ?>" class="validate" placeholder="CI del lector" required>
            <label for="txtCI">CI</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="txtAddress" name="Reader[address]" type="text" value="<?= $data['direccion'] ?>" class="validate" placeholder="Direcci&oacute;n del lector" required>
            <label for="txtAddress">Direcci&oacute;n</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="txtPhoneNumber" name="Reader[phone_number]" type="text" value="<?= $data['telefono'] ?>" class="validate" placeholder="Tel&eacute;fono" required>
            <label for="txtPhoneNumber">Tel&eacute;fono</label>
          </div>
        </div>

        <div class="row">
          <div class="input-field col s12 right-align">
            <button class="btn waves-effect waves-light blue darken-1" type="submit" name="action">
              Guardar
              <i class="material-icons right">save</i>
            </button>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>