<div class="row">
	<div class="col s12 m12">
		<div class="icon-block">
			<h2 class="center light-blue-text"><i class="material-icons">face</i></h2>
			<h5 class="center">Lectores</h5>

			<a href="<?= URL ?>/reader/create" class="btn-small green"><i class="material-icons left">add</i>Nuevo</a>

			<table class="striped">
		        <thead>
		          <tr>
		              <th>Nombre</th>
		              <th>CI</th>
		              <th>Direcci&oacute;n</th>
		              <th>Tel&eacute;fono</th>
		              <th></th>
		          </tr>
		        </thead>
		        <tbody>
					<?php foreach ($data as $d) { ?>
					<tr>
						<td><?= $d['nombre_lector'] ?></td>
						<td><?= $d['ci'] ?></td>
						<td><?= $d['direccion'] ?></td>
						<td><?= $d['telefono'] ?></td>
						<td>
							<a class="btn-small red" href="<?= URL ?>/reader/delete/<?= $d['id_lector'] ?>" title="Eliminar lector" onclick="return confirm('¿Desea completar esta operaci&oacute;n?')"><i class="material-icons">delete_forever</i></a>
							<a class="btn-small" href="<?= URL ?>/reader/update/<?= $d['id_lector'] ?>" title="Editar lector"><i class="material-icons">mode_edit</i></a>
						</td>
					</tr>
					<?php } ?>
		        </tbody>
		    </table>
		</div>
	</div>
</div>