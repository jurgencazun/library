<div class="row">
  <div class="col s12 m12">
    <div class="icon-block">
      <h2 class="center light-blue-text"><i class="material-icons">library_books</i></h2>
      <h5 class="center">Libros / Editar / <?= $data['titulo'] ?></h5>

      <a href="<?= URL ?>/book" class="btn-small"><i class="material-icons left">subdirectory_arrow_left</i>Regresar</a>
    
      <br><br>
      <form class="col s12" method="post" action="<?= URL ?>/book/update/<?= $data['id_libro'] ?>">
        <div class="row">
          <div class="input-field col s12">
            <input id="txtTitle" name="Book[title]" type="text" value="<?= $data['titulo'] ?>" class="validate" placeholder="T&iacute;tulo del libro" required autofocus>
            <label for="txtTitle">T&iacute;tulo</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="txtAuthor" name="Book[author]" type="text" value="<?= $data['autor'] ?>" class="validate" placeholder="Autor del libro" required>
            <label for="txtAuthor">Autor</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="txtGenre" name="Book[genre]" type="text" value="<?= $data['genero'] ?>" class="validate" placeholder="G&eacute;nero del libro" required>
            <label for="txtGenre">G&eacute;nero</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12 right-align">
            <button class="btn waves-effect waves-light blue darken-1" type="submit" name="action">
              Guardar
              <i class="material-icons right">save</i>
            </button>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>