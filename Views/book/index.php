<div class="row">
	<div class="col s12 m12">
		<div class="icon-block">
			<h2 class="center light-blue-text"><i class="material-icons">library_books</i></h2>
			<h5 class="center">Libros</h5>

			<a href="<?= URL ?>/book/create" class="btn-small green"><i class="material-icons left">add</i>Nuevo</a>

			<table class="striped">
		        <thead>
		          <tr>
		              <th>T&iacute;tulo</th>
		              <th>Autor</th>
		              <th>G&eacute;nero</th>
		              <th>Estado</th>
		              <th></th>
		          </tr>
		        </thead>
		        <tbody>
					<?php foreach ($data as $d) { ?>
					<tr>
						<td><?= $d['titulo'] ?></td>
						<td><?= $d['autor'] ?></td>
						<td><?= $d['genero'] ?></td>
						<td><?= $d['prestado'] ? '<span class="new badge grey" data-badge-caption="Pr&eacute;stamo"></span>' : '<span class="new badge green" data-badge-caption="Disponible"></span>' ?></td>
						<td>
							<a class="btn-small red" href="<?= URL ?>/book/delete/<?= $d['id_libro'] ?>" title="Eliminar libro" onclick="return confirm('¿Desea completar esta operaci&oacute;n?')"><i class="material-icons">delete_forever</i></a>
							<a class="btn-small" href="<?= URL ?>/book/update/<?= $d['id_libro'] ?>" title="Editar libro"><i class="material-icons">mode_edit</i></a>
						</td>
					</tr>
					<?php } ?>
		        </tbody>
		    </table>
		</div>
	</div>
</div>