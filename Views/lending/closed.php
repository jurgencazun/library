<div class="row">
	<div class="col s12 m12">
		<div class="icon-block">
			<h2 class="center light-blue-text"><i class="material-icons">all_inclusive</i></h2>
			<h5 class="center">Pr&eacute;stamos (Historial)</h5>
			<h6 class="center"><a href="<?= URL ?>/lending" title="Regresar a lista de pr&eacute;stamos actuales">Pr&eacute;stamos actuales</a></h6>

			<table class="striped">
		        <thead>
		          <tr>
		              <th>T&iacute;tulo</th>
		              <th>Autor</th>
		              <th>Lector actual</th>
		              <th>Fecha pr&eacute;stamo</th>
		              <th>Fecha de devoluci&oacute;n</th>
		          </tr>
		        </thead>
		        <tbody>
					<?php foreach ($data as $d) { ?>
					<tr>
						<td><?= $d['titulo'] ?></td>
						<td><?= $d['autor'] ?></td>
						<td><?= $d['nombre_lector'] ?></td>
						<td><?= date('d/m/Y H:i', strtotime($d['fecha_prestamo'])) ?>h</td>
						<td><?= date('d/m/Y H:i', strtotime($d['fecha_devolucion'])) ?>h</td>
					</tr>
					<?php } ?>
		        </tbody>
		    </table>
		</div>
	</div>
</div>