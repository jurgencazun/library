<div class="row">
	<div class="col s12 m12">
		<div class="icon-block">
			<h2 class="center light-blue-text"><i class="material-icons">all_inclusive</i></h2>
			<h5 class="center">Pr&eacute;stamos</h5>
			<h6 class="center"><a href="<?= URL ?>/lending/closed" title="Ver historial de pr&eacute;stamos">Ver historial</a></h6>

			<a href="<?= URL ?>/lending/create" class="btn-small green"><i class="material-icons left">add</i>Nuevo</a>

			<table class="striped">
		        <thead>
		          <tr>
		              <th>T&iacute;tulo</th>
		              <th>Autor</th>
		              <th>Lector actual</th>
		              <th>Fecha pr&eacute;stamo</th>

		              <th></th>
		          </tr>
		        </thead>
		        <tbody>
					<?php foreach ($data as $d) { ?>
					<tr>
						<td><?= $d['titulo'] ?></td>
						<td><?= $d['autor'] ?></td>
						<td><?= $d['nombre_lector'] ?></td>
						<td><?= date('d/m/Y H:i', strtotime($d['fecha_prestamo'])) ?>h</td>
						<td>
							<a class="btn-small red" href="<?= URL ?>/lending/delete/<?= $d['nro_prestamo'] ?>" title="Eliminar libro" onclick="return confirm('¿Desea completar esta operaci&oacute;n?')"><i class="material-icons">delete_forever</i></a>
							<a class="btn-small" href="<?= URL ?>/lending/update/<?= $d['nro_prestamo'] ?>" title="Editar libro"><i class="material-icons">mode_edit</i></a>
							<a class="btn-small green" href="<?= URL ?>/lending/return/<?= $d['nro_prestamo'] ?>" title="Devolver libro"  onclick="return confirm('¿Desea completar esta operaci&oacute;n?')"><i class="material-icons">undo</i></a>
						</td>
					</tr>
					<?php } ?>
		        </tbody>
		    </table>
		</div>
	</div>
</div>