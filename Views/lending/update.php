<div class="row">
  <div class="col s12 m12">
    <div class="icon-block">
      <h2 class="center light-blue-text"><i class="material-icons">all_inclusive</i></h2>
      <h5 class="center">Libros / Editar / {<?= $data['data']['nombre_lector'] ?> : <?= $data['data']['titulo'] ?> - (<?= $data['data']['autor'] ?>)}</h5>

      <a href="<?= URL ?>/lending" class="btn-small"><i class="material-icons left">subdirectory_arrow_left</i>Regresar</a>
    
      <br><br>
      <form class="col s12" method="post" action="<?= URL ?>/lending/update/<?= $data['data']['nro_prestamo'] ?>">
        <div class="row">
          <div class="input-field col s12">
            <select id="slReader" name="Lending[reader]" class="browser-default">
              <option value="" selected disabled>Seleccione un lector</option>
              <?php foreach ($data['readers'] as $r) { ?>
              <option value="<?= $r['id_lector'] ?>" <?= $r['id_lector'] == $data['data']['id_lector'] ? "selected" : "" ?>><?= $r['nombre_lector'] ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <select id="slBook" name="Lending[book]" class="browser-default">
              <option value="" selected disabled>Seleccione un libro</option>
              <?php foreach ($data['books'] as $r) { ?>
              <option value="<?= $r['id_libro'] ?>" <?= $r['id_libro'] == $data['data']['id_libro'] ? "selected" : "" ?>><?= $r['titulo'] ?> - (<?= $r['autor'] ?>)</option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12 right-align">
            <button class="btn waves-effect waves-light blue darken-1" type="submit" name="action">
              Guardar
              <i class="material-icons right">save</i>
            </button>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>