<?php

namespace Views;

$template = new Template();

class Template{
	public function __construct(){
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>

		<title>Library</title>

		<!-- CSS  -->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="<?= URL ?>/Views/template/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
		<link href="<?= URL ?>/Views/template/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	</head>
	<body>
		
		<nav class="light-blue lighten-1" role="navigation">
			<div class="nav-wrapper container"><a id="logo-container" href="<?= URL ?>" class="brand-logo">
				<img src="<?= URL ?>/Views/template/logo.png">
			</a>
				<ul class="right hide-on-med-and-down">
					<li><a href="<?= URL ?>">Inicio</a></li>
					<li><a href="<?= URL ?>/book">Libros</a></li>
					<li><a href="<?= URL ?>/reader">Lectores</a></li>
					<li><a href="<?= URL ?>/lending">Pr&eacute;stamos</a></li>
				</ul>

				<ul id="nav-mobile" class="sidenav">
					<li><a href="<?= URL ?>">Inicio</a></li>
					<li><a href="<?= URL ?>/book">Libros</a></li>
					<li><a href="<?= URL ?>/reader">Lectores</a></li>
					<li><a href="<?= URL ?>/lending">Pr&eacute;stamos</a></li>
				</ul>
				<a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
			</div>
		</nav>

		<div class="section no-pad-bot" id="index-banner">
			<div class="container">
				
				<?php if(isset($_GET['success'])){ ?>
				<div class="card-panel green">Operaci&oacute;n exitosa.</div>
				<?php } ?>

<?php 
	}

	public function __destruct(){
?>
			</div>
		</div>

		<!--  Scripts-->
		<script src="<?= URL ?>/Views/template/js/jquery-2.1.1.min.js"></script>
		<script src="<?= URL ?>/Views/template/js/materialize.js"></script>
		<script src="<?= URL ?>/Views/template/js/init.js"></script>
	</body>
</html>
<?php
	}
} ?>