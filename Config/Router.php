<?php

namespace Config;

class Router{
	public static function run(Request $request){
		$controller = ucfirst($request->getController()) . "Controller";
		$route = ROOT . "Controllers" . DS . $controller . ".php";
		$method = $request->getMethod();
		$argument = $request->getArgument();
		if(is_readable($route)){
			require_once $route;
			$show = "Controllers\\" . $controller;
			$controller = new $show;
			if(!isset($argument)){
				$data = call_user_func(array($controller, "action". ucfirst($method)));
			}else{
				$data = call_user_func_array(array($controller, "action". ucfirst($method)), $argument);
			}
		}

		$route = ROOT . "Views" . DS . $request->getController() . DS . $request->getMethod() . ".php";
		if(is_readable($route)){
			require_once $route;
		}else{
			print "View " . $request->getMethod() . " not found";
		}
	}
}