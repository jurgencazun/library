DROP TABLE IF EXISTS lector;
CREATE TABLE lector (
  id_lector int(11) NOT NULL AUTO_INCREMENT,
  nombre_lector varchar(100) NOT NULL,
  ci int(11) NOT NULL,
  direccion varchar(200) NOT NULL,
  telefono varchar(20) NOT NULL,
  PRIMARY KEY (id_lector)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

LOCK TABLES lector WRITE;
INSERT INTO lector VALUES (1,'Galileo Galilei',101010,'Florencia, Italia','777777'),(2,'Aristarco de Samos',121212,'Grecia','888888');
UNLOCK TABLES;

DROP TABLE IF EXISTS libro;
CREATE TABLE libro (
  id_libro int(11) NOT NULL AUTO_INCREMENT,
  titulo varchar(50) NOT NULL,
  autor varchar(100) NOT NULL,
  genero varchar(45) NOT NULL,
  prestado bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (id_libro)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;


LOCK TABLES libro WRITE;
INSERT INTO libro VALUES (1,'1984','George Orwell','Novela', 1),(2,'Un mundo feliz','Aldous Huxley','Novela', 0),(3,'Fahrenheit 451','Ray Bradbury','Novela', 0);
UNLOCK TABLES;

DROP TABLE IF EXISTS prestamo;
CREATE TABLE prestamo (
  nro_prestamo int(11) NOT NULL AUTO_INCREMENT,
  id_lector int(11) NOT NULL,
  id_libro int(11) NOT NULL,
  fecha_prestamo datetime NOT NULL,
  fecha_devolucion datetime DEFAULT NULL,
  PRIMARY KEY (nro_prestamo),
  KEY fk_prestamo_lector (id_lector),
  KEY fk_prestamo_libro (id_libro),
  CONSTRAINT fk_prestamo_lector FOREIGN KEY (id_lector) REFERENCES lector (id_lector) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_prestamo_libro FOREIGN KEY (id_libro) REFERENCES libro (id_libro) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

LOCK TABLES prestamo WRITE;
INSERT INTO prestamo VALUES (1,2,1,'2019-05-03 23:15:15','2019-05-03 23:15:58'),(2,1,2,'2019-05-03 23:15:21','2019-05-03 23:15:23'),(3,1,1,'2019-05-03 23:16:28',NULL);
UNLOCK TABLES;

-- ================================================================

DROP TRIGGER IF EXISTS ins_prestamo;
DELIMITER ;;
CREATE TRIGGER ins_prestamo AFTER INSERT ON prestamo
  FOR EACH ROW BEGIN

    UPDATE libro SET prestado = 1 WHERE id_libro = NEW.id_libro;

  END ;;
DELIMITER ;


DROP TRIGGER IF EXISTS upd_prestamo;
DELIMITER ;;
CREATE TRIGGER upd_prestamo AFTER UPDATE ON prestamo
  FOR EACH ROW BEGIN

    IF NOT NEW.fecha_devolucion IS NULL THEN
        UPDATE libro SET prestado = 0 WHERE id_libro = NEW.id_libro;
    ELSE

        IF NOT OLD.id_libro = NEW.id_libro THEN
            UPDATE libro SET prestado = 0 WHERE id_libro = OLD.id_libro;
            UPDATE libro SET prestado = 1 WHERE id_libro = NEW.id_libro;
        END IF;

    END IF;
  END ;;
DELIMITER ;

DROP TRIGGER IF EXISTS del_prestamo;
DELIMITER ;;
CREATE TRIGGER del_prestamo AFTER DELETE ON prestamo
  FOR EACH ROW BEGIN

    UPDATE libro SET prestado = 0 WHERE id_libro = OLD.id_libro;

  END ;;
DELIMITER ;