<?php
namespace Models;

use Models\Connection;
use PDO;

class Lending {
	private $db;
    private $tableName = "prestamo";
    private $nro_prestamo;

    function __construct(){
        $this->db = Connection::connect();
    }

    public function getAll($opened = true){
    	$sql = '
    		SELECT 
			    l.nro_prestamo,
			    b.id_libro,
			    b.titulo,
			    b.autor,
			    r.id_lector,
			    r.nombre_lector,
			    l.fecha_prestamo,
			    l.fecha_devolucion
			FROM
			    ' . $this->tableName . ' l
			        INNER JOIN
			    lector r USING (id_lector)
			        INNER JOIN
			    libro b USING (id_libro)';

        if($opened) {
        	$sql .= ' WHERE l.fecha_devolucion IS NULL';
        }else {
            $sql .= ' WHERE NOT l.fecha_devolucion IS NULL';
        }

        $sql .= ' ORDER BY l.fecha_prestamo DESC;';

        $row = $this->db->query($sql);
        $data = [];
        foreach ($row as $d) {
       		$data[] = $d;
        }

        return $data;
    }

    public function create($data) {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'INSERT INTO ' . $this->tableName . ' (id_lector, id_libro, fecha_prestamo) VALUES (?, ?, NOW());';
        $query = $this->db->prepare($sql);
        $result = $query->execute([$data['reader'], $data['book']]);
        Connection::disconnect();

        return $result;
    }

    public function getData($id) {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $sql = '
            SELECT 
                l.nro_prestamo,
                b.id_libro,
                b.titulo,
                b.autor,
                r.id_lector,
                r.nombre_lector
            FROM
                ' . $this->tableName . ' l
                    INNER JOIN
                lector r USING (id_lector)
                    INNER JOIN
                libro b USING (id_libro)
            WHERE
                l.nro_prestamo = ?;';

        $q = $this->db->prepare($sql);
        $q->execute([$id]);
        $data = $q->fetch(PDO::FETCH_ASSOC);
        $this->nro_prestamo = $data['nro_prestamo'];

        return $data;
    }

    public function update($data){
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = 'UPDATE ' . $this->tableName . ' SET id_lector = ?, id_libro = ? WHERE nro_prestamo = ?;';

        $q = $this->db->prepare($sql);
        $result = $q->execute([$data['reader'], $data['book'], $this->nro_prestamo]);
        Connection::disconnect();

        return $result;
    }

    public function delete($id){
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'DELETE FROM ' . $this->tableName . ' WHERE nro_prestamo = ?;';
        $q = $this->db->prepare($sql);
        $result = $q->execute([$id]);
        Connection::disconnect();

        return $result;
    }

    public function returnBook($id) {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = 'UPDATE ' . $this->tableName . ' SET fecha_devolucion = NOW() WHERE nro_prestamo = ?;';

        $q = $this->db->prepare($sql);
        $result = $q->execute([$id]);
        Connection::disconnect();

        return $result;
    }
}