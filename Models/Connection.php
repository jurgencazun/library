<?php
namespace Models;

class Connection {
    private static $host = 'localhost';
    private static $username = 'root';
    private static $password = 'holamundo';
	private static $database = 'librarydb';
     
    private static $cn  = null;
     
    public function __construct() {
        die('Init function is not allowed');
    }
     
    public static function connect() {
    	if (null == self::$cn) {     
        	try{
        		self::$cn =  new \PDO( "mysql:host=" . self::$host . ";" . "dbname=" . self::$database, self::$username, self::$password); 
        	} catch(\PDOException $e) {
          		die($e->getMessage());
        	}
      	}
    	
    	return self::$cn;
    }
     
    public static function disconnect() {
        self::$cn = null;
    }
}