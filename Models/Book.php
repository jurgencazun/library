<?php
namespace Models;

use Models\Connection;
use PDO;

class Book {
	private $db;
    private $tableName = "libro";
    private $id_libro;

	function __construct(){
        $this->db = Connection::connect();
    }

    public function getAll($where = ''){
        $sql = 'SELECT * FROM ' . $this->tableName . ' ' . $where . ' ORDER BY titulo ASC;';

        $row = $this->db->query($sql);
        $data = [];
        foreach ($row as $d) {
       		$data[] = $d;
        }

        return $data;
    }

    public function create($data) {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'INSERT INTO ' . $this->tableName . ' (titulo, autor, genero) VALUES (?, ?, ?);';
        $query = $this->db->prepare($sql);
        $result = $query->execute([$data['title'], $data['author'], $data['genre']]);
        Connection::disconnect();

        return $result;
    }

    public function getData($id) {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'SELECT id_libro, titulo, autor, genero FROM ' . $this->tableName . ' WHERE id_libro = ?;';
        $q = $this->db->prepare($sql);
        $q->execute([$id]);
        $data = $q->fetch(PDO::FETCH_ASSOC);
        $this->id_libro = $data['id_libro'];
        return $data;
    }

    public function update($data){
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'UPDATE ' . $this->tableName . ' SET titulo = ?, autor = ?, genero = ? WHERE id_libro = ?;';
        $q = $this->db->prepare($sql);
        $result = $q->execute([$data['title'], $data['author'], $data['genre'], $this->id_libro]);
        Connection::disconnect();

        return $result;
    }

    public function delete($id){
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'DELETE FROM ' . $this->tableName . ' WHERE id_libro = ?;';
        $q = $this->db->prepare($sql);
        $result = $q->execute([$id]);
        Connection::disconnect();

        return $result;
    }
}