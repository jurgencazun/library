<?php
namespace Models;

use Models\Connection;
use PDO;

class Reader {
	private $db;
    private $tableName = "lector";
    private $id_lector;

	function __construct(){
        $this->db = Connection::connect();
    }

    public function getAll(){
        $sql = 'SELECT * FROM ' . $this->tableName . ' ORDER BY nombre_lector ASC;';
        $row = $this->db->query($sql);
        $data = [];
        foreach ($row as $d) {
       		$data[] = $d;
        }

        return $data;
    }

    public function create($data) {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'INSERT INTO ' . $this->tableName . ' (nombre_lector, ci, direccion, telefono) VALUES (?, ?, ?, ?);';
        $query = $this->db->prepare($sql);
        $result = $query->execute([$data['name'], $data['ci'], $data['address'], $data['phone_number']]);
        Connection::disconnect();

        return $result;
    }

    public function getData($id) {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'SELECT id_lector, nombre_lector, ci, direccion, telefono FROM ' . $this->tableName . ' WHERE id_lector = ?;';
        $q = $this->db->prepare($sql);
        $q->execute([$id]);
        $data = $q->fetch(PDO::FETCH_ASSOC);
        $this->id_lector = $data['id_lector'];
        return $data;
    }

    public function update($data){
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'UPDATE ' . $this->tableName . ' SET nombre_lector = ?, ci = ?, direccion = ?, telefono = ? WHERE id_lector = ?;';
        $q = $this->db->prepare($sql);
        $result = $q->execute([$data['name'], $data['ci'], $data['address'], $data['phone_number'], $this->id_lector]);
        Connection::disconnect();

        return $result;
    }

    public function delete($id){
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'DELETE FROM ' . $this->tableName . ' WHERE id_lector = ?;';
        $q = $this->db->prepare($sql);
        $result = $q->execute([$id]);
        Connection::disconnect();

        return $result;
    }
}