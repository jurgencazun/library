<?php

namespace Controllers;

use Models\Book;

class BookController {
	public function actionIndex(){
		$model = new Book;
		$list = $model->getAll();

		return $list;
	}

	public function actionCreate(){
		if(isset($_POST['Book'])) {
			$model = new Book;
			if($model->create($_POST['Book'])) {
				header("location: " . URL . "/book?success");
			} else {
				print '<div class="card-panel red">No fue posible completar la operaci&oacute;n</div>';
			}
		}
	}

	public function actionUpdate($id) {
		$model = new Book;
		$data = $model->getData($id);

		if(isset($_POST['Book'])) {
			if($model->update($_POST['Book'])) {
				header("location: " . URL . "/book?success");
			} else {
				print '<div class="card-panel red">No fue posible completar la operaci&oacute;n</div>';
			}
		}

		return $data;
	}

	public function actionDelete($id) {
		$model = new Book;

		if($model->delete($id)) {
			header("location: " . URL . "/book?success");
		} else {
			print '<div class="card-panel red">No fue posible completar la operaci&oacute;n</div>';
		}
	}
}