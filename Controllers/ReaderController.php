<?php

namespace Controllers;

use Models\Reader;

class ReaderController {
	public function actionIndex(){
		$model = new Reader;
		$list = $model->getAll();

		return $list;
	}

	public function actionCreate(){
		if(isset($_POST['Reader'])) {
			$model = new Reader;
			if($model->create($_POST['Reader'])) {
				header("location: " . URL . "/reader?success");
			} else {
				print '<div class="card-panel red">No fue posible completar la operaci&oacute;n</div>';
			}
		}
	}

	public function actionUpdate($id) {
		$model = new Reader;
		$data = $model->getData($id);

		if(isset($_POST['Reader'])) {
			if($model->update($_POST['Reader'])) {
				header("location: " . URL . "/reader?success");
			} else {
				print '<div class="card-panel red">No fue posible completar la operaci&oacute;n</div>';
			}
		}

		return $data;
	}

	public function actionDelete($id) {
		$model = new Reader;

		if($model->delete($id)) {
			header("location: " . URL . "/reader?success");
		} else {
			print '<div class="card-panel red">No fue posible completar la operaci&oacute;n</div>';
		}
	}
}