<?php

namespace Controllers;

use Models\Lending;
use Models\Reader;
use Models\Book;

class LendingController {
	public function actionIndex(){
		$model = new Lending;
		$list = $model->getAll();

		return $list;
	}

	public function actionCreate(){
		if(isset($_POST['Lending'])) {
			$model = new Lending;
			if($model->create($_POST['Lending'])) {
				header("location: " . URL . "/lending?success");
			} else {
				print '<div class="card-panel red">No fue posible completar la operaci&oacute;n</div>';
			}
		}

		$readers = new Reader;
		$readers = $readers->getAll();

		$books = new Book;
		$books = $books->getAll("WHERE NOT prestado");

		return [
			'readers' => $readers,
			'books' => $books
		];
	}

	public function actionUpdate($id){
		$model = new Lending;
		$data = $model->getData($id);

		if(isset($_POST['Lending'])) {
			if($model->update($_POST['Lending'])) {
				header("location: " . URL . "/lending?success");
			} else {
				print '<div class="card-panel red">No fue posible completar la operaci&oacute;n</div>';
			}
		}

		$readers = new Reader;
		$readers = $readers->getAll();

		$books = new Book;
		$books = $books->getAll("WHERE NOT prestado OR id_libro = " . $data['id_libro']);

		return [
			'data' => $data,
			'readers' => $readers,
			'books' => $books
		];
	}

	public function actionDelete($id) {
		$model = new Lending;

		if($model->delete($id)) {
			header("location: " . URL . "/lending?success");
		} else {
			print '<div class="card-panel red">No fue posible completar la operaci&oacute;n</div>';
		}
	}

	public function actionReturn($id) {
		$model = new Lending;
		if($model->returnBook($id)) {
			header("location: " . URL . "/lending?success");
		} else {
			print '<div class="card-panel red">No fue posible completar la operaci&oacute;n</div>';
		}
	}

	public function actionClosed() {
		$model = new Lending;
		$list = $model->getAll(false);

		return $list;
	}
}